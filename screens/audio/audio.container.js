/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-22 15:32:18
 * @modify date 2019-07-22 15:32:18
 * @desc [description]
*/

import React from 'react';
import * as FileSystem from 'expo-file-system';
import AudioComponent from './audio';
import uniqueId from './../../unique';
import { Toast } from 'native-base';

export default class Audio extends React.Component {

    static navigationOptions = (({navigation}) => ({
        title: 'Recordings',
        headerStyle: {
            backgroundColor: '#1f3975',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'normal',
        },
        headerMode: 'screen'
    }));

    constructor() {

        super();

        this.state = {
            recordings: []
        }

    }

    async componentDidMount() {

        try {
            const recordings = await FileSystem.readDirectoryAsync(`${FileSystem.documentDirectory}Audio`);
            this.setState({
                recordings: this._structureRecords(recordings)
            })
        }
        catch(e) {
            console.log(e);
        }

    }

    _structureRecords = (recordings) => {
        return recordings ? recordings.map(recording =>  {
            return {
                title: recording,
                playedPercentage: 0,
                isPlaying: false,
                currentPlaybackPosition: 0,
                uniqueId: uniqueId()
            }
        }) : []
    }

    _setIsPlaying = (index, isPlaying = false) => {

        if(index < 0) return;

        let {recordings} = this.state;

        recordings[index].isPlaying = isPlaying;

        this.setState({
            recordings
        });

    }

    _setCurrentPlaybackPosition = (index, position = 0) => {

        if(index < 0) return;

        let {recordings} = this.state;

        recordings[index].currentPlaybackPosition = position;

        this.setState({
            recordings
        });

    }

    _setPlayedPercentage = (index, percentage = 0) => {

        if(index < 0) return;

        let {recordings} = this.state;

        recordings[index].playedPercentage = percentage;

        this.setState({
            recordings
        });

    }

    _deleteAudio = (audio = []) => {

        let deletedItems = audio.filter(async ({title}) => {
            try {
                await FileSystem.deleteAsync(`${FileSystem.documentDirectory}Audio/${title}`);
                return true;
            }
            catch(e) {
                console.error(e);
                return false;
            }
            
        }).map(({uniqueId}) => uniqueId);

        let {recordings} = this.state;

        recordings = recordings.filter(({uniqueId}) => !deletedItems.includes(uniqueId));

        if(deletedItems.length === audio.length) {
            Toast.show({
                text: 'Deleted Successfully',
                buttonText: 'Okay',
                duration: 3000,
                type: 'success'
            });
        }
        else {
            Toast.show({
                text: 'Some items were not deleted.',
                buttonText: 'Okay',
                duration: 3000,
                type: 'danger'
            });
        }

        this.setState({
            recordings
        }); 

        return recordings;

    }

    _rename = async (audio, newName) => {

        if(!audio.title || !newName) return;

        try {
            await FileSystem.moveAsync({
                from: `${FileSystem.documentDirectory}Audio/${audio.title}`,
                to: `${FileSystem.documentDirectory}Audio/${newName}`
            });
            
            let {recordings} = this.state;

            recordings = recordings.map((record) => {
                if(audio.title === record.title) {
                    console.log({...record,title: newName}, "checkingnnnn");
                    return {
                        ...record,
                        title: newName,
                    };
                }
                else {
                    return record;
                }
            });

            console.log(recordings);

            this.setState({
                recordings
            });

            return true;
        }
        catch(e) {
            console.error(e);
            return false;
        }

    }

    render() {

        const {recordings} = this.state;

        return <AudioComponent  
            _setIsPlaying={this._setIsPlaying}
            _setCurrentPlaybackPosition={this._setCurrentPlaybackPosition}
            _setPlayedPercentage={this._setPlayedPercentage}
            _deleteAudio={this._deleteAudio}
            _rename={this._rename}
            
            recordings={recordings}
            navigate={this.props.navigation.navigate}
        />;

    }

}