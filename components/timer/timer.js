/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-21 15:58:22
 * @modify date 2019-07-21 15:58:22
 * @desc [description]
*/

import React from 'react';
import {View, Text} from 'react-native';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';

// import { Worker } from 'react-native-workers';

const STOPPED = 0;
const RECORDING = 1;
const PAUSED = 2;

const STYLES = StyleSheet.create({
    timer: {
        flex: 1,
        flexDirection: 'row'
    }
});

export default class Timer extends React.Component {

    t = undefined;

    constructor() {
        super();
        this.state = {
            selfUpdated:  false,
            timer: {
                h: 0,
                m: 0,
                s: 0
            }
        }

    }

    componentDidMount() {

        // let worker = new Worker('../../workers/timer.js');
        // worker.postMessage("yud ladi du");

        this._handleTimer();
    }

    componentWillUnmount() {
        this._stopTimer();
    }

    componentDidUpdate(prevProps) {

        if(prevProps.action === this.props.action) return;

        this._handleTimer();
        
    }

    _handleTimer = () => {
        switch(this.props.action) {

            case STOPPED:
                this._stopTimer();
            break;

            case PAUSED:
                this._pauseTimer();
            break;

            case RECORDING:
                this._startTimer();
            break;

        }
    }

    _startTimer = () => {
        this.t = setInterval(() => {

            let {timer} = this.state;

            if(timer.s && timer.s % 60 === 0) {
                timer.m = timer.m + 1;
                timer.s = 0;

                if(timer.m && timer.m % 60 === 0) {
                    timer.h = timer.h + 1;
                    timer.m = 0;
                }
            }
            else{
                timer.s = timer.s + 1;
            }

            this.setState({
                timer,
            });
        }, 1000);
    }

    _stopTimer = () => {
        clearInterval(this.t);
        this.setState({
            timer: {h:0, m:0, s:0},
        });
    }

    _pauseTimer = () => {
        clearInterval(this.t);
    }

    render() {

        let {h,m,s} = this.state.timer;

        if(h < 10) {
            h = '0' + h;
        }
        if(m < 10) {
            m = '0' + m;
        }
        if(s < 10) {
            s = '0' + s;
        }

        return (
            <View style={STYLES.timer}>
                <Text>{h} :</Text>
                <Text>{m} :</Text>
                <Text>{s}</Text>
            </View>
        )

    }

}

Timer.propTypes = {
    action: PropTypes.number.isRequired
}