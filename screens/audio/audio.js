/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-22 15:31:49
 * @modify date 2019-07-22 15:31:49
 * @desc [description]
*/

import React from 'react';
import { Audio as AudioExpo } from 'expo-av';
import * as FileSystem from 'expo-file-system';
import { Container, Content, View, List, ListItem, Text, Icon, Header, Left, Body, Title, ActionSheet, Button, Input, Item, Toast } from 'native-base';
import ProgressCircle from 'react-native-progress-circle';

import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';

let actionsheetOptions = [
    {text: 'Rename', icon: 'md-brush', iconColor: '#7f8c8d'},
    {text: 'Favorite', icon: 'star', iconColor: '#f1c40f'},
    {text: 'Delete', icon: 'trash', iconColor: '#e74c3c'},
    {text: 'Cancel', icon: 'ios-close'},
];

let cancelButtonIndex = 3;
let destructiveButtonIndex = 2;

export default class Audio extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            records: [],
            playingAudioIndex: -1,
            renaming: {
                renameIndex: -1,
                newName: undefined
            }
        }

    }

    _play = async (audio, playFrom, index) => {

        try {
            
            if(this.sound){

                if(this.state.playingAudioIndex !== index) {
                    await this._pause(this.state.playingAudioIndex);
                    await this.sound.unloadAsync();
                    await this.sound.loadAsync({uri: `${FileSystem.documentDirectory}Audio/${audio}`});
                }

            }

            else {
                this.sound = await new AudioExpo.Sound();
                await this.sound.loadAsync({uri: `${FileSystem.documentDirectory}Audio/${audio}`});
            }

            // await this.sound.setStatusAsync({
            //     positionMillis: playFrom
            // });

            await this.sound.playAsync();

            this.sound.setProgressUpdateIntervalAsync(250);
            this.sound.setOnPlaybackStatusUpdate(this._onPlayBackStatusUpdate);

            this.setState({
                playingAudioIndex: index
            });

            // passed down from container - audio.container.js
            this.props._setIsPlaying(index, true);
        }
        catch(e) {
            console.log(e);
        }

    }

    _pause = async (index) => {

        try {
            await this.sound.pauseAsync();
            // passed down from container - audio.container.js
            this.props._setIsPlaying(index, false);
        }
        catch(e) {
            console.log(e);
        }
    }

    _resume = async (index) => {
        try {
            await this.sound.playAsync();
            // passed down from container - audio.container.js
            this.props._setIsPlaying(index, true);
        }
        catch(e) {
            console.log(e);
        }
    }

    _stop = async (index) => {

        if(!index) index = this.state.playingAudioIndex;

        try {
            await this.sound.stopAsync();
            await this.sound.unloadAsync();
            // passed down from container - audio.container.js
            this.props._setIsPlaying(index, false);
        }
        catch(e) {
            console.log(e);
        }
    }

    _onPlayBackStatusUpdate = ({durationMillis, positionMillis, isPlaying}) => {

        if(!isPlaying) return;

        let playedPercentage = this._calculatePlayedPercentage(durationMillis, positionMillis);

        if(playedPercentage === 100) {
            this._stop();

            return;
        }
            
        this.props._setPlayedPercentage(this.state.playingAudioIndex, playedPercentage);
        this.props._setCurrentPlaybackPosition(this.state.playingAudioIndex,positionMillis);


    }

    _calculatePlayedPercentage = (totalDuration, playedDuration) => {

        if(totalDuration <= 0) return 0;

        return playedDuration/totalDuration * 100;

    }

    _performActionSheetAction = (buttonIndex, itemIndex) => {

        switch(buttonIndex) {

            case 0:
                this._showRenameField(itemIndex);
                break;

            case 1:
                // mark as favorite
                break;
            case 2:
                
                this.props._deleteAudio([this.props.recordings[itemIndex]]);

                break;
            case 3:
                break;

            default:

        }

    }

    _beginSelection = (selectedIndex = 0) => {

        this.props.navigate('Selection', {
            items: this.props.recordings.map(({title, uniqueId}, i) => { 
                return { 
                    title, 
                    uniqueId, 
                    selected: selectedIndex === i
                }
            }),
            _deleteAudio: this.props._deleteAudio
        });

    }

    // to define input box
    _showRenameField = (itemIndex) => {

        let {renaming} = this.state;

        renaming.renameIndex = itemIndex;

        this.setState({
            renaming
        });

    }

    _cancelRename = () => {

        let {renaming} = this.state;
        renaming.renameIndex = -1;

        this.setState({
            renaming
        });
    }

    _updateNewName = (value) => {

        let {renaming} = this.state;
        renaming.newName = value;

        this.setState({
            renaming
        });
    }

    _rename = () => {

        const {renaming: {renameIndex, newName}} = this.state;
        const {recordings} = this.props;

        // returns bool
        if(this.props._rename(recordings[renameIndex], newName)){
            this.setState({
                renaming: {renameIndex: -1, newName: undefined}
            })
        }
        else{
            Toast.show({
                type: 'danger',
                text: 'Could not rename',
                duration: 5000,
                buttonText: 'Okay'
            });
        }

    }

    render() {

        const {recordings} = this.props;
        const {renameIndex, newName} = this.state.renaming;

        // console.log(recordings);

        return (
            <Container>
                <Content>
                    <List>
                        {
                            recordings.map((record, i) => (
                                renameIndex === i ? 
                                    <Item key={record.uniqueId} style={STYLES.input}>
                                        <Input placeholder={record.title} autoFocus onChangeText={(value) => this._updateNewName(value) } value={newName} />
                                        <Icon name='ios-checkmark-circle' style={{color:'#2ecc71'}} onPress={this._rename}/>
                                        <Icon name='ios-close-circle' style={{color: '#e74c3c'}} onPress={this._cancelRename}/>
                                    </Item>
                                :
                                <ListItem noIndent style={STYLES.listItem} key={record.uniqueId} onLongPress={() => this._beginSelection(i)}>
                                    <View style={STYLES.iconView}>
                                        <Button transparent onPress={() => {
                                            ActionSheet.show(
                                                {
                                                    options: actionsheetOptions,
                                                    cancelButtonIndex,
                                                    destructiveButtonIndex,
                                                    title: 'Actions',
                                                },
                                                buttonIndex => {
                                                    this._performActionSheetAction(buttonIndex, i);
                                                }
                                            );
                                        }} >
                                            <Icon name='md-more' />
                                        </Button>
                                        <Text>{record.title}</Text>
                                    </View>
                                    <ProgressCircle
                                        percent={record.playedPercentage}
                                        radius={15}
                                        borderWidth={2}
                                        color="#3399FF"
                                        shadowColor="#999"
                                        bgColor="#fff"
                                    >
                                        {
                                            record.isPlaying ? 
                                                <Icon style={{fontSize: 15}} name='pause' onPress={() => this._pause(i)} />
                                                :
                                                <Icon style={{fontSize: 15}} name='play' onPress={() => this._play(record.title, record.currentPlaybackPosition, i)} />
                                        }
                                    </ProgressCircle>
                                </ListItem>
                            ))
                        }
                    </List>
                </Content>
            </Container>
        );

    }

}

Audio.propTypes = {
    recordings: PropTypes.array.isRequired
}

const STYLES = StyleSheet.create({
    listItem: {
        justifyContent: 'space-between',
        flex: 1,
        paddingLeft: 4
    },
    iconView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    progress: {
        position: 'absolute',
        left: 0,
        top: 0,
        height: '100%',
        width: '100%',
        backgroundColor: 'lightgray',
        padding: 12,
        paddingLeft: 18,
        margin: 0
    },
    input: {
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 18,
        fontSize: 10,
        backgroundColor: '#ecf0f1'
    }
});