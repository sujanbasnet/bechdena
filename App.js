import React from 'react';
import { StyleSheet, StatusBar, SafeAreaView, Platform } from 'react-native';
import { StyleProvider, Root, Toast } from 'native-base';

import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

import Roboto from 'native-base/Fonts/Roboto.ttf';
import Roboto_medium from 'native-base/Fonts/Roboto_medium.ttf';

import platform from './native-base-theme/variables/platform';
import getTheme from './native-base-theme/components';

import Navigation from './navigation';

import * as Permissions from 'expo-permissions';
import { AppLoading } from 'expo';

export default class App extends React.Component {

	constructor() {
		super();
		this.state = {
			loadingFonts: true,
			permissions: {
				audio: false,
				video: false
			}
		}
	}

	async componentDidMount() {
		await Font.loadAsync({
			Roboto,
			Roboto_medium,
			...Ionicons.font,
		});

		this.setState({
			loadingFonts: false
		});

		this._askPermission();

	}

	_askPermission = async () => {
		const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING);	

		if(status !== 'granted') {
			try {
				Toast.show({
					text: "app needs permission to record audio",
					button: "okay",
					position: "top"
				});
			}
			catch(e) {
				console.warn(e);
			}
		}

	}

	render() {

		const {loadingFonts} = this.state;

		if(loadingFonts) return <AppLoading />;

		return (
			<Root>
			{/* // <SafeAreaView style={styles.safeAreaView}> */}
				<StyleProvider style={getTheme(platform)}>
				{/* // 	<Container style={styles.safeAreaView}> */}
						<Navigation />
				{/* // 	</Container> */}
				</StyleProvider>
			{/* // </SafeAreaView> */}
			</Root>
		);

	}
}

const styles = StyleSheet.create({
	"safeAreaView": {
		"paddingTop": Platform.OS === 'android' ? StatusBar.currentHeight : 0,
		"backgroundColor": platform.statusBarColor
    }
});