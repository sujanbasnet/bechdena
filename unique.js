/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-09-04 15:54:07
 * @modify date 2019-09-04 15:54:07
 * @desc [description]
*/

let ids = [];

const uniqueId = () => {

    let uniqueId = `${Date.now()}_${Math.round(Math.random()*1000)}_${ids.length}`;
    ids.push(uniqueId);

    return uniqueId;

}

export const clearIds = () => {
    ids = [];
}

export default uniqueId;