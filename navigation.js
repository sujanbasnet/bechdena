/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-07 16:15:23
 * @modify date 2019-07-07 16:15:23
 * @desc [description]
*/

import React from 'react';

import Home from './screens/home/home.container';
import Record from './screens/record/record.container';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import Audio from './screens/audio/audio.container';
import SelectionScreen from './screens/selection/selection';

const MainNav = createStackNavigator({
	Audio: {
		screen: Audio
	},
	Record: {
		screen: Record
	},
	Selection: {
		screen: SelectionScreen
	}
},{
	initialRouteName: 'Record',
	// headerMode: 'none'
});

export default createAppContainer(MainNav);