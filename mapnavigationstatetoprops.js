/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-09-03 12:51:40
 * @modify date 2019-09-03 12:51:40
 * @desc [description]
*/

import React from 'react';

const mapNavigationStateToProps = (component) => class extends React.Component {

    static navigationOptions = component.navigationOptions;

    render() {
        
        const {navigation: {state: {params}}} = this.props;

        return <component {...params} {...this.props} />;
    }

}

export default mapNavigationStateToProps;