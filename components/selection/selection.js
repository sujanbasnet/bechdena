/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-09-03 12:06:12
 * @modify date 2019-09-03 12:06:12
 * @desc [description]
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Container, Content, View, List, Text, ListItem, Header, Body, Title, Left, Icon, Button, Right } from 'native-base';
import { StyleSheet, StatusBar } from 'react-native';

export default class Selection extends React.Component {

    constructor() {
        super();

        this.state = {
            items: [],
            allSelected: false
        }
    }

    componentDidMount() {

        console.log(this.props);
        this.setState({
            items: this.props.items ? this.props.items : []
        });
    }

    _selectAll = () => {

        let {items} = this.state;

        items.forEach((item) => {
            item.selected = true;
        });

        this.setState({
            items,
            allSelected: true
        });

    }

    _deselectAll = () => {
        
        let {items} = this.state;

        items.forEach((item) => {
            item.selected = false;
        });

        this.setState({
            items,
            allSelected: false
        });

    }

    _toggleSelect = (index) => {

        let {items} = this.state;

        items[index].selected = !items[index].selected;

        this.setState({
            items,
        });

    }

    _deleteAudio = () => {
        let items = this.props._deleteAudio(this.state.items.filter(({selected}) => (
            selected
        )));

        this.setState({
            items
        });
    }

    render() {

        const {items, allSelected} = this.state;
        const { navigation: {goBack} } = this.props;

        return (

            <Container>
                <Header style={STYLES.header}>
                    <Left>
                        <Button transparent onPress={() => goBack()}>
                            <Icon name='arrow-back' style={STYLES.iconBlack} />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Recordings</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={this._deleteAudio}>
                            <Icon name='trash' style={STYLES.iconBlack} />
                        </Button>
                        <Button transparent onPress={allSelected ? this._deselectAll : this._selectAll}>
                            <Icon name={allSelected ? 'ios-checkbox' : 'ios-checkbox-outline'} style={STYLES.iconBlack} />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <View>
                        <List>
                            {
                                items && items.map(({title, uniqueId, selected}, i) => (
                                    <ListItem 
                                        noIndent 
                                        key={uniqueId} 
                                        selected={selected} 
                                        style={[selected ? STYLES.selected : null, STYLES.listItems]} 
                                        onPress={() => this._toggleSelect(i)}
                                    >
                                        <Text>{title}</Text>
                                    </ListItem>
                                ))
                            }
                        </List>
                    </View>
                </Content>
            </Container>

        );

    }

}

Selection.defaultProps = {
    selected: false
}

Selection.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        uniqueId: PropTypes.string.isRequired,
    }))
};

const STYLES = StyleSheet.create({
    header: {
        height:100,
        paddingTop: StatusBar.currentHeight,
    },
    selected: {
        backgroundColor: 'rgba(0,0,0,0.165)'
    },
    listItems: {
        paddingTop: 17.5,
        paddingBottom: 17.5
    },
    iconBlack: {
        color: '#222f3e'
    }
});