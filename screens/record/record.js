/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-19 15:08:17
 * @modify date 2019-07-19 15:08:17
 * @desc [description]
*/

import React from 'react';
import { Text, Container, Button, Icon, Footer, FooterTab, Body, Title, Content, Input } from 'native-base';

import { StyleSheet, View, Modal } from 'react-native';
import { Audio } from 'expo-av';
import Timer from '../../components/timer/timer';
import * as FileSystem  from 'expo-file-system';

const STOPPED = 0;
const RECORDING = 1;
const PAUSED = 2;

export default class Record extends React.Component {

    constructor() {
        super();
        this.state = {
            recordStatus: STOPPED,
        }
        this.recording = null;

    }

    _prepareForRecording = async () => {

        this.recording = new Audio.Recording();

        try {
            await this.recording.prepareToRecordAsync(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);
        }
        catch(e) {
            console.error(e);
        }

    }

    _stopRecording = async () => {

        try {
            await this.recording.stopAndUnloadAsync();
            
            this._saveRecording();
            this.setState({
                recordStatus: STOPPED
            });
        }
        catch(e) {
            console.error(e);
        }

        
    }

    _pauseRecording = async () => {

        try {
            await this.recording.pauseAsync();

            this.setState({
                recordStatus: PAUSED
            });
        }
        catch(e) {
            console.error(e);
        }
        
    }

    _resumeRecording = async () => {
        try {
            await this.recording.startAsync();

            this.setState({
                recordStatus: RECORDING
            });

        }
        catch(e) {
            console.log(e);
        }
    }

    _startRecording = async () => {

        if(this.recording === null) await this._prepareForRecording();

        if(this.state.recordStatus === RECORDING) return;

        try {
            await this.recording.startAsync();

            this.setState({
                recordStatus: RECORDING
            });

        }
        catch(e) {
            console.log(e);
        }

    }

    _saveRecording = async () => {

        try {
            await FileSystem.copyAsync({
                from : this.recording.getURI(),
                to: `${FileSystem.documentDirectory}Audio/recording_${new Date().getTime()}`
            });

            // stopAndUnloadAsync removes recording instance
            // assign null to create a new instance for new recording
            this.recording = null;  
        }
        catch(e) {
            console.error(e);
        }
    }

    render() {

        const {recordStatus} = this.state;
        const {navigate} = this.props.navigation;

        return (

            <Container>
                <Content padder contentContainerStyle={STYLES.content}>
                    <View style={STYLES.container}>
                        <Button onPress={this._startRecording} rounded style={STYLES.recordButton}>
                            <Icon name='mic' style={STYLES.icon} />
                        </Button>

                        {
                            recordStatus !== STOPPED ?
                                <Timer action={recordStatus} />
                                :
                                null
                        }

                        {
                            recordStatus === RECORDING ?
                                <View style={STYLES.buttonGroup}>
                                    {
                                        recordStatus === PAUSED ? 
                                            <Button onPress={this._resumeRecording} transparent>
                                                <Icon name="play" />
                                            </Button>
                                            :
                                            <Button onPress={this._pauseRecording} transparent>
                                                <Icon name="pause" />
                                            </Button>
                                    }
                                    <Button onPress={this._stopRecording} transparent>
                                        <Icon name="ios-square" />
                                    </Button>
                                </View>
                                :
                                null
                        }

                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button full onPress={() => navigate('Audio')}>
                            <Text>All Recordings</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>

        )

    }

}

const STYLES = StyleSheet.create({
    content: {
        flex: 1
    },  
    container: {
        flex: 1,
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    recordButton: {
        alignSelf: 'center',
        backgroundColor: '#e74c3c',
    },
    icon: {
        color: "white"
    },
    buttonGroup: {
        flex: 1,
        flexDirection: 'row'
    }
});