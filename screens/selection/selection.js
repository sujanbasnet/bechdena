/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-09-03 15:29:05
 * @modify date 2019-09-03 15:29:05
 * @desc [description]
*/

import React from 'react';
import Selection from "../../components/selection/selection";
import { Animated, Easing } from 'react-native';
import { Button, Text } from 'native-base';

export default class SelectionScreen extends React.Component {

    static navigationOptions = (({navigation}) => ({
        // title: 'Recordings',
        header: null,
        // headerStyle: {
        //     backgroundColor: '#1f3975',
        // },
        // headerTintColor: '#fff',
        // headerTitleStyle: {
        //     fontWeight: 'normal',
        // },
        // headerRight: <Button><Text>Delete</Text></Button>,
        transitionConfig : () => ({
            transitionSpec: {
                duration: 0,
                timing: Animated.timing,
                easing: Easing.step0,
            },
        }),
    }));

    render() {

        const {navigation: {state : {params}}} = this.props;

        return <Selection {...params} {...this.props} />;

    }

}