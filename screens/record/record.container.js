/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-19 15:08:49
 * @modify date 2019-07-19 15:08:49
 * @desc [description]
*/

import React from 'react';
import RecordComponent from './record';

export default class Record extends React.Component {

    static navigationOptions = (({navigation}) => ({
        title: 'Record Audio',
        headerStyle: {
            backgroundColor: '#1f3975',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'normal',
        },
    }))

    render() {

        return <RecordComponent navigation={this.props.navigation} />

    }

}