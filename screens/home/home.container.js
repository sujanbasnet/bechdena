/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-06-26 13:42:44
 * @modify date 2019-06-26 13:42:44
 * @desc [description]
*/

import React from 'react';

import HomeComponent from './home';
import { Image } from 'react-native';
import {StyleSheet} from 'react-native';

import background from '../../assets/Rectangle 2.png';

export default class Home extends React.Component {

    static navigationOptions = {
        title: 'Home',
        headerTintColor: '#fff',
        headerTitleStyle: {
        fontWeight: 'bold',
        },
    }

    render() {

        return <HomeComponent />

    }

}