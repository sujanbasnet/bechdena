/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-06-26 13:41:54
 * @modify date 2019-06-26 13:41:54
 * @desc [description]
*/

import React from 'react';
import { Container, Footer, Header, Content, Text, Input, Card, CardItem, Icon, Body, Item } from 'native-base';

import { StyleSheet, Image, ScrollView } from 'react-native';
import { theme } from '../../theme/theme';

const styles = StyleSheet.create({
    "container" : {
        "backgroundColor": "#fff"
    },
    "header" : {
        "backgroundColor": theme.color.primary
    }
});

export default class Home extends React.Component {

    static navigationOtions = (({navigation}) => ({
        title: 'Home',
        headerStyle: {
            backgroundColor: '#f4511e',
        },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold',
        },
    }))

    render() {

        return (
            // <StyleProvider style={getTheme(platform)}>
                <Container style={styles.container}>
                    {/* <Header searchBar rounded>
                        <Item>
                            <Icon name="ios-search" />
                            <Input placeholder="Search" />
                            <Icon name="ios-people" />
                        </Item>
                    </Header> */}
                    <Content padder>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            <Card>
                                <CardItem header>
                                    <Text>NativeBase</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Image style={{width: 66, height: 58}} source={{uri: '../../assets/splash.png'}} />
                                    </Body>
                                </CardItem>
                                <CardItem footer>
                                    <Text>GeekyAnts</Text>
                                </CardItem>
                            </Card>
                            <Card>
                                <CardItem header>
                                    <Text>NativeBase</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Image source={{uri: '../../assets/splash.png'}} />
                                    </Body>
                                </CardItem>
                                <CardItem footer>
                                    <Text>GeekyAnts</Text>
                                </CardItem>
                            </Card>
                            <Card>
                                <CardItem header>
                                    <Text>NativeBase</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Image source={{uri: '../../assets/splash.png'}} />
                                    </Body>
                                </CardItem>
                                <CardItem footer>
                                    <Text>GeekyAnts</Text>
                                </CardItem>
                            </Card>
                            <Card>
                                <CardItem header>
                                    <Text>NativeBase</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Image source={{uri: '../../assets/splash.png'}} />
                                    </Body>
                                </CardItem>
                                <CardItem footer>
                                    <Text>GeekyAnts</Text>
                                </CardItem>
                            </Card>
                            <Card>
                                <CardItem header>
                                    <Text>NativeBase</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Image source={{uri: '../../assets/splash.png'}} />
                                    </Body>
                                </CardItem>
                                <CardItem footer>
                                    <Text>GeekyAnts</Text>
                                </CardItem>
                            </Card>
                            <Card>
                                <CardItem header>
                                    <Text>NativeBase</Text>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <Image source={{uri: '../../assets/splash.png'}} />
                                    </Body>
                                </CardItem>
                                <CardItem footer>
                                    <Text>GeekyAnts</Text>
                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                    <Footer></Footer>
                </Container>
            // </StyleProvider>
        );

    }

}