/**
 * @author [Sujan Basnet]
 * @email [basnetsujan95@gmail.com]
 * @create date 2019-07-03 12:03:04
 * @modify date 2019-07-03 12:03:04
 * @desc [description]
*/

import { StyleSheet } from 'react-native';

export const theme = {

    "color": {
        "primary" : "#084A91",
        "secondary": "#e5842b"
    }

};